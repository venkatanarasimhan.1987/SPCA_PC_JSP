<%-- 
    Document   : updateAnimal
    Created on : 22-Dec-2015, 10:18:37 PM
    Author     : Venkata
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>       
        <link rel="stylesheet" type="text/css" href="style.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Update Animal Records</title>
    </head>
    <%
        Connection con = DriverManager.getConnection(
                "jdbc:mysql://myspcadb.clkcyrhcrnlf.us-west-2.rds.amazonaws.com:3306/MySPCADB",
                "administrator", "administrator");
        String sql = "SELECT animal_id,animal_name,animal_type,animal_gender,animal_desc,"
                + "age,house_type,pet_frndly,cost FROM animal";
        PreparedStatement ps = con.prepareStatement(sql);
        ResultSet rst = ps.executeQuery();
        int i = 0;
        int animal_id = 0;
        int animal_name = 0;
        int animal_type = 0;
        int animal_gender = 0;
        int animal_desc = 0;
        int animal_age = 0;
        int animal_houseType = 0;
        int animal_petFriendly = 0;
        int animal_cost = 0;
    %>
    <body>
        <h1>Update Only 1 Animal at a time</h1>
        <h4>Animal List</h4>
        <form name=myname method=post
              action="updateSuccess.jsp">
            <table border="1">
                <tr><td><span style="font-weight:bold">Select</span></td>
                    <td><span style="font-weight:bold">Animal ID</span></td>
                    <td><span style="font-weight:bold">Animal Name</span></td>
                    <td><span style="font-weight:bold">Animal Type</span></td>
                    <td><span style="font-weight:bold">Animal Gender</span></td>
                    <td><span style="font-weight:bold">Animal Description</span></td>
                    <td><span style="font-weight:bold">Animal Age</span></td>
                    <td><span style="font-weight:bold">Animal House Type</span></td>
                    <td><span style="font-weight:bold">Pet Friendly</span></td>
                    <td><span style="font-weight:bold">Cost</span></td>
                </tr>
                <%
            while (rst.next()) {%>
                <tr>
                    <td><input type="checkbox" name="check<%=i%>"
                               value=<%=rst.getInt(1)%></td>
                    <td><input type="TEXT" name="animal_id<%=animal_id%>"
                               value="<%=rst.getString(1)%>" readOnly></td>
                    <td><input type="TEXT" name="animal_name<%=animal_name%>" 
                               value="<%=rst.getString(2)%>"></td>
                    <td><input type="TEXT" name="animal_type<%=animal_type%>"
                               value="<%=rst.getString(3)%>"></td>
                    <td><input type="TEXT" name="animal_gender<%=animal_gender%>"
                               value="<%=rst.getString(4)%>"></td>
                    <td><input type="TEXT" name="animal_desc<%=animal_desc%>"
                               value="<%=rst.getString(5)%>"></td>
                    <td><input type="TEXT" name="animal_age<%=animal_age%>"
                               value="<%=rst.getInt(6)%>"></td>
                    <td><input type="TEXT" name="animal_houseType<%=animal_houseType%>"
                               value="<%=rst.getString(7)%>"></td>
                    <td><input type="TEXT" name="animal_petFreindly<%=animal_petFriendly%>"
                               value="<%=rst.getString(8)%>"></td>
                    <td><input type="TEXT" name="animal_cost<%=animal_cost%>"
                               value="<%=rst.getInt(9)%>"></td>
                </tr>
                <%}%>
            </table>
            <br>
            <br>
            <br>
        <input style="font-weight:bold"
                    type="submit"
                    value="Update">
        </form>
            <br>
            <br>
            <br>
            <form name=myname method=post
                  action="updateSuccess.jsp">
            </form>
    </body>
</html>
