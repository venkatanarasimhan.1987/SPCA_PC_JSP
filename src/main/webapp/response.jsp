<%-- 
    Document   : response
    Created on : 27-Sep-2015, 8:21:57 PM
    Author     : Venkata
--%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.io.IOException"%>
<%@page import="javax.sql.DataSource"%>
<%@page import="javax.annotation.Resource"%>
<%@page import="javax.servlet.annotation.WebServlet"%>
<%@page import="java.sql.Blob"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>

    <head>
        <link rel="stylesheet" type="text/css" href="style.css">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    </head>



    <body>
        <sql:query var="counsSubjRs" maxRows="1" dataSource="jdbc/SPCAPhaseOne">
            SELECT * FROM animal WHERE animal_id= ? <sql:param value="${param.animal_id_ref}"/>
        </sql:query>
        <c:set var="counsSubj" scope="request" value="${counsSubjRs.rows[0]}"/>
        <h1>SPCA - Animal Information</h1>

        <table border="1">

            <thead>

                <tr>

                    <th colspan="2" id="animal_id" > <strong>Animal ID: </strong>${counsSubj.animal_id}</th>

                </tr>

            </thead>

            <tbody>

                <tr>

                    <td><strong>Animal Name: </strong></td>

                    <td><strong><span style="font-size:smaller; font-style:italic; ">${counsSubj.animal_name}</span></strong></td>

                </tr>

                <tr>

                    <td><strong>Animal Age: </strong></td>

                    <td><strong><span style="font-size:smaller; font-style:italic; ">${counsSubj.age}</span></strong></td>

                </tr>

                <tr>

                    <td><strong>Animal Description: </strong></td>

                    <td><strong><span style="font-size:smaller; font-style:italic; ">${counsSubj.animal_desc}</span></strong></td>

                </tr>

                <tr>

                    <td><strong>House Type: </strong></td>

                    <td><strong><span style="font-size:smaller; font-style:italic; ">${counsSubj.house_type}</span></strong></td>

                </tr>

                <tr>

                    <td><strong>Animal Gender: </strong></td>

                    <td><strong><span style="font-size:smaller; font-style:italic; ">${counsSubj.animal_gender}</span></strong></td>

                </tr>

                <tr>

                    <td><strong>Pet Friendly status: </strong></td>

                    <td><strong><span style="font-size:smaller; font-style:italic; ">${counsSubj.pet_frndly}</span></strong></td>

                </tr>



                <tr>

                    <td><strong>Animal Cost: </strong></td>

                    <td><strong><span style="font-size:smaller; font-style:italic; ">${counsSubj.cost}</span></strong></td>

                </tr>

                <tr>
                    <td><strong>Image: </strong></td>  

                    <td>
                        <form action="showImage.jsp">
                            <form>
                                <input type="submit" value="Show Image"  name=${counsSubj.animal_id} />                             
                        </form>


                    </td>  
                </tr>

            </tbody>

        </table>        

    </body>
</html>
