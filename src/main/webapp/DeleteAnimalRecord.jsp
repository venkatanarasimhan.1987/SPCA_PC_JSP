<%-- 
    Document   : DeleteAnimalRecord
    Created on : 20-Dec-2015, 6:38:24 PM
    Author     : Venkata
--%>

<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>Page To Delete Animal Records</title>
    </head>
    <%
        Connection con = DriverManager.getConnection(
                "jdbc:mysql://myspcadb.clkcyrhcrnlf.us-west-2.rds.amazonaws.com:3306/MySPCADB",
                "administrator", "administrator");
        String sql = "SELECT animal_id,animal_name,animal_desc,cost FROM animal";
        PreparedStatement ps = con.prepareStatement(sql);
        ResultSet rst = ps.executeQuery();
        int i = 0;
    %>
    <body>
        <h1>Delete Only 1 Animal at a time</h1>
        <h3>OR Choose delete all.</h3>
        <h4> List of Users</h4>
        <form name=myname method=post
              action="deleteSuccess.jsp">
            <table border="1">
                <tr><td><span style="font-weight:bold">Select</span></td>
                    <td><span style="font-weight:bold">Animal ID</span></td>
                    <td><span style="font-weight:bold">Animal Name</span></td>
                    <td><span style="font-weight:bold">Animal Description</span></td>
                    <td><span style="font-weight:bold">Cost</span></td>
                </tr>
                <%
            while (rst.next()) {%>
                <tr>
                    <td><input type="checkbox" name="check<%=i%>"
                               value=<%=rst.getInt(1)%></td>
                    <td><%=rst.getInt(1)%></td>
                    <td><%=rst.getString(2)%></td>
                    <td><%=rst.getString(3)%></td>
                    <td><%=rst.getInt(4)%></td>
                </tr>
                <%}%>
            </table>
            <br>
            <br>
            <br>
            <input style="font-weight:bold"
                    type="submit"
                    value="Delete">
        </form>
            <br>
            <br>
            <br>
            <form name=myname method=post
                  action="deleteSuccess.jsp">
                <input style="font-weight:bold"
                    type="submit"
                    value="Delete All">
            </form>
    </body>
</html>
