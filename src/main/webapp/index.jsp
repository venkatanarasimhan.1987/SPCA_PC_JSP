<%-- 
    Document   : index
    Created on : 27-Sep-2015, 12:19:54 PM
    Author     : Venkata
--%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.*" %>
<%@page import="java.io.*" %> 
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <link rel="stylesheet" type="text/css" href="style.css">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SPCA Page</title>
    </head>
    <body>

        <sql:query var="animals" dataSource="jdbc/SPCAPhaseOne">
            SELECT animal_id FROM animal
        </sql:query>

        <h1>Animal Table</h1>
        <br>
        <form action="response.jsp">
 
            <strong>Select an Animal :</strong>
            <br>
            <select name="animal_id_ref">
                <c:forEach var="animal" items="${animals.rows}">

                    <option value="${animal.animal_id}">${animal.animal_id}</option>

                </c:forEach>              
            </select>

            <br>
            <br>
            <form>
                <input type="submit" name="submit_fordetails" value="Check Details" /> 
            </form> 
           
            <br>
            <br>


            <H1>All Animal List </H1>

            <%
                Connection connection = DriverManager.getConnection(
                        "jdbc:mysql://myspcadb.clkcyrhcrnlf.us-west-2.rds.amazonaws.com:3306/MySPCADB", "administrator", "administrator");

                Statement statement = connection.createStatement();
                ResultSet resultset
                        = statement.executeQuery("select * from animal");
            %>

            <TABLE BORDER="1">
                <TR>
                    <TH>Pet ID</TH>
                    <TH>Pet Name</TH>
                    <TH>Pet Type</TH>
                    <TH>Gender</TH>
                    <TH>Description</TH>
                </TR>
                <%
                    if (resultset.next()) {
                        resultset.beforeFirst();  // for making sure you dont miss the first record.
                        while (resultset.next()) {                           
                %>
                <TR>
                    <TD> <%= resultset.getString(1)%></td>
                    <TD> <%= resultset.getString(2)%></TD>
                    <TD> <%= resultset.getString(3)%></TD>
                    <TD> <%= resultset.getString(4)%></TD>
                    <TD> <%= resultset.getString(5)%></TD>                  
                </TR>
                <%
                   
                        }
                    }
                %>
            </TABLE>


            <br>
            <br>
            <form action="NewAnimalPage.jsp">
                <form>
                    <input type="submit" name="submit_newrecord" value="Add New Animal Record" />
                </form>

                <br>
                <br>
            <form action="DeleteAnimalRecord.jsp">
                <form>
                    <input type="submit" name="submit_deleterecord" value="Delete Animal Record" />
                </form>
                <br>
                <br>
                
            <form action="updateAnimal.jsp">
                <form>
                    <input type="submit" name="submit_toDB" value="Update Animal" />
                </form>
                <br>
                <br>
                
                </body>
        </html>
