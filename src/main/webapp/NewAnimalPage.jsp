<%-- 
    Document   : NewAnimalPage
    Created on : Sep 29, 2015, 3:53:02 PM
    Author     : Venkata
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <H1>New Animal info </H1>
        <form method="post" action="FileUploadDBServlet" enctype="multipart/form-data">
        <TABLE BORDER="2">
           
            <TR>
                <td>Animal Name: </td>
                <td><INPUT TYPE="TEXT" NAME="animal_name"> </td>
            </TR>

            <TR>
                <td>Animal Type: </td>
                <td>                  
                    <select NAME="animal_type">                        
                        <option>Default</option>
                        <option>Dog</option>
                        <option>Cat</option>
                        <option>Other</option>
                    </select>
                </td>
            </TR>

            <TR>                
                <td>Animal Gender:</td>
                <td><select NAME="animal_gender">
                        <option>Default</option>
                        <option>M</option>
                        <option>F</option>
                    </select></td>
            </TR>

            <TR>
                <td>Animal Description: </td>
                <td><textarea NAME="animal_desc">
                    
                    </textarea></td>
            </TR>

            <TR>
                <td>Animal Age:  </td>
                <td><select NAME="age">
                        <option>Default</option>
                        <option>0</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                        <option>9</option>
                        <option>10</option>
                        <option>11</option>
                        <option>12</option>
                        <option>13</option>
                        <option>14</option>
                        <option>15</option>
                        <option>16</option>
                    </select></td>
            </TR>

            <TR>
                <td>House Type:  </td>
                <td><select NAME="house_type">
                        <option>Default</option>
                        <option>Condo</option>
                        <option>House</option>
                    </select></td>
            </TR>
            
            <TR>
                <td>Pet Friendly:  </td>
                <td><select NAME="pet_frndly">
                        <option>Default</option>
                        <option>Yes</option>
                        <option>No</option>
                    </select></td>
            </TR>

            <TR>
                <td>Animal Cost:  </td>
                <td><select NAME="cost">
                        <option>Default</option>
                        <option>295</option>
                        <option>225</option>
                        <option>100</option>
                        <option>195</option>
                        <option>155</option>
                        <option>85</option>
                    </select></td>
            </TR>

            <TR>
                <td>Photo:</td>
                <td><input type="file" name="photo"/></td>
            </TR>    

        </TABLE>

        <br>
        <br>
        <form>
            <input type="submit" name="submit_toDB" value="Add New Animal Record" />
        </form>
    </body>
</html>
