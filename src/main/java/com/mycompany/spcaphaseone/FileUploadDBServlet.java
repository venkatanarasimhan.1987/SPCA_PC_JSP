/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spcaphaseone;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import java.io.IOException;
import java.io.InputStream;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Venkata
 */
@WebServlet(name = "FileUploadDBServlet", urlPatterns = {"/FileUploadDBServlet"})
@MultipartConfig(maxFileSize = 16177215)    // upload file's size up to 16MB
public class FileUploadDBServlet extends HttpServlet {

    
    // database connection settings
    private String dbURL = "jdbc:mysql://myspcadb.clkcyrhcrnlf.us-west-2.rds.amazonaws.com:3306/MySPCADB";
    private String dbUser = "administrator";
    private String dbPass = "administrator";
    
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String animal_name = request.getParameter("animal_name");
        String animal_type = request.getParameter("animal_type");
        String animal_gender = request.getParameter("animal_gender");
        String animal_desc = request.getParameter("animal_desc");
        String age = request.getParameter("age");
        String house_type = request.getParameter("house_type");
        String pet_frndly = request.getParameter("pet_frndly");
        String cost = request.getParameter("cost");
        
        
        InputStream inputStream = null; // input stream of the upload file
         
        // obtains the upload file part in this multipart request
        Part filePart = request.getPart("photo");
        if (filePart != null) {
            // prints out some information for debugging
            System.out.println(filePart.getName());
            System.out.println(filePart.getSize());
            System.out.println(filePart.getContentType());
             
            // obtains input stream of the upload file
            inputStream = filePart.getInputStream();
        }
         
        Connection conn = null; // connection to the database
        String message = null;  // message will be sent back to client
        
        try {
            // connects to the database
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            conn = (Connection) DriverManager.getConnection(dbURL, dbUser, dbPass);
 
            // constructs SQL statement
            String sql = "INSERT INTO animal (animal_name,animal_type,animal_gender,animal_desc,age,house_type,pet_frndly,cost,image) VALUES (?,?,?,?,?,?,?,?,?)";
            PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql);
            statement.setString(1, animal_name);
            statement.setString(2, animal_type);
            statement.setString(3, animal_gender);
            statement.setString(4, animal_desc);
            statement.setString(5, age);
            statement.setString(6, house_type);
            statement.setString(7, pet_frndly);
            statement.setString(8, cost);
            
             
            if (inputStream != null) {
                // fetches input stream of the upload file for the blob column
                statement.setBlob(9, inputStream);
            }
 
            // sends the statement to the database server
            int row = statement.executeUpdate();
            if (row > 0) {
                message = "File uploaded and saved into database";
            }
        } catch (SQLException ex) {
            message = "ERROR: " + ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                // closes the database connection
                try {
                    conn.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            // sets the message in request scope
            request.setAttribute("Message", message);
             
            // forwards to the message page
            getServletContext().getRequestDispatcher("/Message.jsp").forward(request, response);
        }
        
    }

        
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
