/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spcaphaseone;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import java.io.IOException;
import java.io.InputStream;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
/**
 *
 * @author Venkata
 */
@WebServlet(name = "FileDeleteDBServlet", urlPatterns = {"/FileDeleteDBServlet"})
@MultipartConfig(maxFileSize = 16177215)    // upload file's size up to 16MB
public class FileDeleteDBServlet extends HttpServlet{
    
    // database connection settings
    private String dbURL = "jdbc:mysql://myspcadb.clkcyrhcrnlf.us-west-2.rds.amazonaws.com:3306/MySPCADB";
    private String dbUser = "administrator";
    private String dbPass = "administrator";
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String animal_id = request.getParameter("animal_id");
        Connection conn = null; // connection to the database
        String message = null;  // message will be sent back to client
        
        try {
            // connects to the database
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            conn = (Connection) DriverManager.getConnection(dbURL, dbUser, dbPass);
 
            // constructs SQL statement
            String sql = "DELETE FROM animal WHERE animal_id = ?";
            PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql);
            System.out.println("The animal id is --->" + animal_id);
            statement.setString(1,animal_id);
        } catch (SQLException ex) {
            message = "ERROR: " + ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                // closes the database connection
                try {
                    conn.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            // sets the message in request scope
            request.setAttribute("Message", message);
             
            // forwards to the message page
            getServletContext().getRequestDispatcher("/Message.jsp").forward(request, response);
        }
        
    }

        
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    
}
